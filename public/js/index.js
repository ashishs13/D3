function d3Graphs(){
	var width = 600,
	    height = 300,
	    radius = Math.min(width, height) / 2,
	    transformWidth = width/2,
	    transformHeight = height/2,
	    div = d3.select("body").append("div").attr("class", "toolTip");

	var color = d3.scale.category20();

	var pie = d3.layout.pie()
	    .value((d) => {
	    	return d[Object.keys(d)]; 
	    })
	    .sort(null);
	var arc = d3.svg.arc()
	    .innerRadius(radius - 100)
	    .outerRadius(radius - 20);

	var svg = d3.select("body").append("svg")
	    .attr("width", width)
	    .attr("height", height)
		.append("g")
	    .attr("transform", `translate(${transformWidth/2} ,${transformHeight})`);


	var color_hash = {  0 : ["A", "red"],
	                    1 : ["B", "green"],
	                    2 : ["C", "Yellow"],
	                    3 : ["D", "orange"],
	                    4 : ["F", "blue"],
	                    5 : ["G", "pink"],
	                    6 : ["H", "grey"],
	                    7 : ["I", "purple"],
	                    8 : ["J", "Coral"],
	                    9 : ["K", "Crimson"],
	                    10 : ["L", "DarkOliveGreen"],
	                    11 : ["M", "DarkSeaGreen"],
	                    12 : ["N", "DarkRed"],
	                    13 : ["O", "DarkKhaki"],
	                    14 : ["P", "Chocolate"],
	                    15 : ["Q", "Black"]
	                  }  


	return(() =>{
		console.log(color_hash)
		var action = () => {
			return new Promise((resolve,reject) => {
				d3.json("sample.json",(error,data) => {
					data = data.trials.status;
					resolve(data)

					var path = svg.datum(data).selectAll("path")
						.data(pie)
						.enter().append("path")
						.attr("fill",(d, i) => { 
							return color(i);
							 })
						.attr("d", arc);

					path.on("mousemove",(d)=> {
						div.style("left", d3.event.pageX+10+"px");
						div.style("top", d3.event.pageY-25+"px");
						div.style("display", "inline-block");
						div.html((Object.keys(d.data))+"<br>"+(d.data[Object.keys(d.data)]));
					});
					path.on("mouseout",(d)=> {
		    			div.style("display", "none");
					});

					var g = svg.selectAll(".arc")
				        .data(pie(data))
				        .enter().append("g")
				        .attr("class", "arc");

			            g.append("text")
				        .attr("transform", (d) => {
				        return "translate(" + arc.centroid(d) + ")";
				    })
					.attr("dy", ".35em")
			        .style("text-anchor", "middle")
			        .text((d) => {
				    		return d.value;
					})
					
				})
			});	
		}

		action()
			.then((data) => {
			var legend = svg.append("g")
				.attr("class","legend")
				.attr("height", 100)
				.attr("width", 100)
				.attr('transform', 'translate(-250,-50)')  

				legend.selectAll('rect')
					.data(data)
					.enter()
					.append("rect")
					.attr("x", width - 120)
		    		.attr("y", function(d, i){return(i*20);})
		    		.attr("width",10)
		    		.attr("height",10)
		    		.style("fill",(d)=>{
		        		return(color_hash[data.indexOf(d)][1])
		    		});

				legend.selectAll('text')
					.data(data)
					.enter()
					.append("text")
					.attr("x",width - 105)
					.attr("y", function(d, i){ return i *  20 + 9;})
				    .text(function(d) {
				        var text = Object.keys(d);
				        return text;
		      		});
			})
			.catch((err)=>{
				console.log("Error"+err);
			});
	})
}	

d3Graphs()()


